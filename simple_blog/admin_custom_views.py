import csv

from django import forms
from django.http import HttpResponse
from django.views.generic import FormView

from simple_blog.models import Comment


class CSVExportForm(forms.forms.Form):
    post_id = forms.IntegerField(min_value=1)


class ExportCommentsAdminView(FormView):
    form_class = CSVExportForm
    template_name = 'export_view.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        post_id = self.request.path.split('/')[-2]

        # Manually plugging in context variables is needed to fullfill django template values!
        context['title'] = 'Export comments for related post!'
        context['post_id'] = post_id

        return context

    def form_valid(self, form):
        field_names = (
            'id',
            'author__username',
            'related_post_id',
            'title',
            'text',
            'created_date',
            'published_date',
        )
        related_coments_data = Comment.objects.filter(
            related_post=form.cleaned_data['post_id'],
        ).values(*field_names)
        related_coments_data = list(related_coments_data)
        for data_row in related_coments_data:
            created_date = data_row['created_date']
            published_date = data_row['published_date']
            data_row['created_date'] = created_date.isoformat() if created_date else None
            data_row['published_date'] = published_date.isoformat() if published_date else None

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="one.csv"'
        writer = csv.DictWriter(response, fieldnames=field_names, delimiter=',')
        writer.writeheader()
        writer.writerows(related_coments_data)

        return response
