import datetime

from django.contrib import admin
from django.contrib.admin.options import csrf_protect_m

from simple_blog.models import Post, Comment


@admin.action(description='Mark selected posts as published')
def make_published(modeladmin, request, queryset):
    queryset.update(published_date=datetime.datetime.utcnow())


class CommentsInline(admin.StackedInline):
    model = Comment
    readonly_fields = (
        'author',
        'created_date',
    )

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def has_add_permission(self, request, obj):
        return False


class AdminPost(admin.ModelAdmin):
    change_form_template = 'change_form.html'

    list_display = [
        'id',
        'author',
        'title',
        'get_text',
        'created_date',
        'published_date',
    ]
    list_filter = [
        'created_date',
        'published_date',
    ]
    search_fields = [
        'id',
        'title',
        'text',
    ]
    readonly_fields = (
        'author',
        'created_date',
    )
    actions = [make_published]
    inlines = [CommentsInline]

    def get_text(self, obj):
        value = obj.text or ''
        if len(value) > 103:
            return value[:100] + '...'
        return value

    get_text.short_description = 'Text'

    # def get_queryset(self, request):
    #     qs = super().get_queryset(request)
    #     qs = qs.filter(comments__isnull=False)
    #     return qs

    # def has_view_permission(self, request, obj=None):
    #     return False

    # def has_change_permission(self, request, obj=None):
    #     return False
    #
    # def has_delete_permission(self, request, obj=None):
    #     not_published = obj.published_date is None if obj else True
    #     return request.user.email in ('marcin.kurzeja@booksy.com',) or not_published
    #
    # def has_add_permission(self, request):
    #     return False

    # def get_readonly_fields(self, request, obj=None):
    #     readonly_fields = list(super().get_readonly_fields(request, obj=obj))
    #     if Comment.objects.filter(related_post_id=obj.id).exists():
    #         readonly_fields.append('title')
    #         readonly_fields.append('text')
    #     return readonly_fields

    @csrf_protect_m
    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        if extra_context is None:
            extra_context = {}
        if object_id is not None:
            extra_context['show_comments_actions'] = Comment.objects.filter(
                related_post_id=object_id,
            ).exists()
        return super().changeform_view(
            request=request,
            object_id=object_id,
            form_url=form_url,
            extra_context=extra_context,
        )


class AdminComment(admin.ModelAdmin):
    list_display = (
        'id',
        'author',
        'title',
        'related_post_id',
    )
    search_fields = (
        '=related_post__id',
    )


admin.site.register(Post, AdminPost)
admin.site.register(Comment, AdminComment)
